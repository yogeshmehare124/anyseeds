-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 13, 2022 at 11:54 AM
-- Server version: 10.5.12-MariaDB
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id16348605_seeds`
--

-- --------------------------------------------------------

--
-- Table structure for table `flowerseeds`
--

CREATE TABLE `flowerseeds` (
  `id` int(10) NOT NULL,
  `product_code` int(10) NOT NULL,
  `product_name` varchar(60) NOT NULL,
  `product_desc` tinytext NOT NULL,
  `product_img_name` varchar(120) NOT NULL,
  `qty` int(5) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flowerseeds`
--

INSERT INTO `flowerseeds` (`id`, `product_code`, `product_name`, `product_desc`, `product_img_name`, `qty`, `price`) VALUES
(1, 1, 'Amaranthus tricolour mixed - Desi Seeds', '\r\nDecorated your garden with an attractive foliage Amaranthus plant. The seed packet contains approx. 35 seeds.', 'nurserylive-amaranthus-tricolour-mixed_220x220.jpg', 500, 55.00),
(2, 2, 'Amaranthus cadatus red - Desi Seeds', '\r\nDecorated your garden with an annual flowering Amaranthus plant. The seed packet contains approx. 35 seeds', 'nurserylive-amaranthus-cadatus-red_220x220.jpg', 500, 55.00),
(3, 3, 'Balsam double mixed - Desi Seeds', 'Balsam flower resemble with mini roses thickly spaced petals and tones. The seed packet contains 35 seeds.', 'nurserylive-balsam-double-mixed_220x220.jpg', 500, 55.00),
(4, 4, 'Balsam rose, Balsam gulab - Desi Seeds', 'Balsam flower resemble with mini roses thickly spaced petals and tones. The seed packet contains approx. 35 seeds', 'nurserylive-balsam-rose-balsam-gulab_220x220.jpg', 500, 55.00),
(5, 5, 'Balsam single - Desi Seeds', '\r\nBalsam flower resemble with mini roses thickly spaced petals and tones. The seed packet contains 35 seeds.', 'nurserylive-balsam-single_220x220.jpg', 500, 55.00),
(6, 6, 'Celosia plumosa mixed - Desi Seeds', 'Gomphrena produces globular flowerheads that brighten the garden. Seed Packet contains 35 seeds.', 'nurserylive-celosia-plumosa-mixed_220x220.jpg', 500, 55.00),
(7, 7, 'Papaya Selection I - Seeds', '\r\n1 packet contains 30 seeds of papaya.', 'nurserylive-papaya.png', 500, 115.00),
(8, 8, 'Foot Kachri - Seeds', '\r\n1 packet contains Foot Kachri - 50 seeds.', 'nurserylive-foot-kachri-seeds.jpg', 500, 115.00),
(9, 9, 'Musk Melon F1 Hybrid Mithas- Seeds', '1 packet contains Musk Melon F1 Hybrid Mithas - 30 seeds.', 'nurserylive-musk-melon.png', 500, 115.00),
(10, 10, 'Cape Gooseberry, Rasbhari - Seeds', '1 packet contains Cape Gooseberry - 50 seeds.', 'nurserylive-cape-gossberry.png', 500, 145.00),
(11, 11, 'More Easy To Grow Vegetable Seeds - ( 14 Seeds Pack )', '\r\nEasy To Grow Vegetable Seeds Pack contains healthy 14 veggies seeds.', 'nurserylive-easy-to-grow-vegetable-seeds-14-seeds-pack_220x220.jpg', 500, 805.00),
(12, 12, 'Easy To Grow Herb Seeds - ( 11 Seeds Pack )', 'Easy To Grow Herb Seeds Pack has 11 amazing different herb seeds.', 'nurserylive-easy-to-grow-herb-seeds-11-seeds-pack_220x220.jpg', 500, 738.00),
(13, 13, 'Easy To Grow Flower Seeds - ( 9 Seeds Pack ) - Seeds', '\r\nThis plantscapping pack contains 9 items worth Rs 1125', 'nurserylive-easy-to-grow-flower-seeds-(9seeds-pack)_220x220.jpg', 500, 563.00),
(14, 14, 'Fruit Seeds - ( 9 Seeds Pack )', 'Fruit Seeds Pack contains nutritious 9 different fruit seeds. Make your daily diet healthy with this pack', 'nurserylive-fruit-seeds-9-seeds-pack_220x220.jpg', 500, 639.00),
(15, 15, 'Turnip White Special No. 1 - Seeds', '1 packet contains Turnip White Special No. 1 - 5gm seeds.', 'nurserylive-turnip-white-special-no-1_220x220.jpg', 0, 105.00),
(16, 16, 'Water Melon F1 Aalam Daksh 55 - Seeds', 'Watermelon is a delicious and refreshing fruit \r\n1 packet contains - 3gm / 10gm seeds. ', 'wamelon_220x220.jpg', 500, 115.00),
(17, 17, 'Cherry Tomato, Cherry Tomato Honey - Seeds', 'Growing your own tomatoes is simple, just a couple of plants will reward you with plenty of tomatoes in the summer.', 'nurserylive-cherry-tomato_220x220.png', 500, 115.00),
(18, 18, 'Kale Fringed Leaves, Ornamental Cabbage - Seeds', '\r\nCabbage\r\n\r\n1 packet contains Cabbage - 50 seeds. ', 'ornamental-cabbage_220x220.jpg', 500, 115.00),
(19, 19, 'Snake Gourd Chichinda - Seeds', 'Snake gourd can easily grow in containers. The seed packet contains approximately 35 seeds.', 'snake-gourd_220x220.jpg', 500, 115.00),
(20, 20, 'Celery Imported, Celery Tall Utah - seeds', '\r\nCelery is a long-season crop that can be tricky to grow, some might say, the trickiest of all. 1 packet contains 100 seeds of celery.', 'celery_220x220.png', 500, 115.00);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(15) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_desc` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `units` int(5) NOT NULL,
  `total` int(15) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `email` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_code`, `product_name`, `product_desc`, `price`, `units`, `total`, `date`, `email`) VALUES
(27, '11', 'More Easy To Grow Vegetable Seeds - ( 14 Seeds Pack )', '\r\nEasy To Grow Vegetable Seeds Pack contains healthy 14 veggies seeds.', 805, 3, 2415, '2021-03-10 16:09:08', 'yo@gmail.com'),
(28, '1', 'Amaranthus tricolour mixed - Desi Seeds', '\r\nDecorated your garden with an attractive foliage Amaranthus plant. The seed packet contains approx. 35 seeds.', 55, 3, 165, '2021-03-13 03:48:39', 'yogeshmehare@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pin` int(6) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(15) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `address`, `city`, `pin`, `email`, `password`, `type`) VALUES
(3, 'yolb', 'uryi', 'dsjkl', 'kjl/h', 846, 'ym@gmail.com', 'yoyo', 'user'),
(5, 'yo', 'yo', 'yo', 'yo', 0, 'yo@gmail.com', '0000', 'user'),
(6, 'chetali', 'mehare', 'yooy', 'ngp', 0, 'ch@gmail.com', '0000', 'user'),
(7, 'yo', 'me', '124', 'ngp', 1234, 'yome@gmail.com', '0000', 'user'),
(8, 'yo', 'me', '124', 'ngp', 123456, 'yogeshmehare@gmail.com', '0000', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flowerseeds`
--
ALTER TABLE `flowerseeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pdt_code` (`product_code`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flowerseeds`
--
ALTER TABLE `flowerseeds`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
